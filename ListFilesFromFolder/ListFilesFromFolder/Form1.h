#pragma once

#include < stdio.h >
#include < stdlib.h >
#include < vcclr.h >


namespace ListFilesFromFolder {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Resumen de Form1
	///
	/// ADVERTENCIA: si cambia el nombre de esta clase, deber� cambiar la
	///          propiedad 'Nombre de archivos de recursos' de la herramienta de compilaci�n de recursos administrados
	///          asociada con todos los archivos .resx de los que depende esta clase. De lo contrario,
	///          los dise�adores no podr�n interactuar correctamente con los
	///          recursos adaptados asociados con este formulario.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			this->m_fileList = gcnew System::Collections::ArrayList;
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n utilizando.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Collections::ArrayList^ m_fileList;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  lb_file_save;
	protected: 

	private: System::Windows::Forms::Label^  lb_folder;
	private: System::Windows::Forms::Button^  bt_help_save_file;


	private: System::Windows::Forms::Button^  bt_help_folder;
	private: System::Windows::Forms::Button^  bt_close;
	private: System::Windows::Forms::Button^  bt_list_files;



	private: System::Windows::Forms::TextBox^  txb_file_save;

	private: System::Windows::Forms::TextBox^  txb_folder;

	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::ProgressBar^  progressBar1;

	private:
		/// <summary>
		/// Variable del dise�ador requerida.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido del m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->txb_file_save = (gcnew System::Windows::Forms::TextBox());
			this->txb_folder = (gcnew System::Windows::Forms::TextBox());
			this->lb_file_save = (gcnew System::Windows::Forms::Label());
			this->lb_folder = (gcnew System::Windows::Forms::Label());
			this->bt_help_save_file = (gcnew System::Windows::Forms::Button());
			this->bt_help_folder = (gcnew System::Windows::Forms::Button());
			this->bt_close = (gcnew System::Windows::Forms::Button());
			this->bt_list_files = (gcnew System::Windows::Forms::Button());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->txb_file_save);
			this->groupBox1->Controls->Add(this->txb_folder);
			this->groupBox1->Controls->Add(this->lb_file_save);
			this->groupBox1->Controls->Add(this->lb_folder);
			this->groupBox1->Controls->Add(this->bt_help_save_file);
			this->groupBox1->Controls->Add(this->bt_help_folder);
			this->groupBox1->Location = System::Drawing::Point(12, 15);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(652, 110);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Parametros de Ejecuci�n";
			// 
			// txb_file_save
			// 
			this->txb_file_save->Font = (gcnew System::Drawing::Font(L"Arial", 12));
			this->txb_file_save->ForeColor = System::Drawing::SystemColors::MenuHighlight;
			this->txb_file_save->Location = System::Drawing::Point(199, 64);
			this->txb_file_save->Name = L"txb_file_save";
			this->txb_file_save->Size = System::Drawing::Size(410, 26);
			this->txb_file_save->TabIndex = 5;
			// 
			// txb_folder
			// 
			this->txb_folder->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->txb_folder->ForeColor = System::Drawing::Color::DarkGreen;
			this->txb_folder->Location = System::Drawing::Point(199, 32);
			this->txb_folder->Name = L"txb_folder";
			this->txb_folder->Size = System::Drawing::Size(410, 26);
			this->txb_folder->TabIndex = 4;
			// 
			// lb_file_save
			// 
			this->lb_file_save->AutoSize = true;
			this->lb_file_save->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lb_file_save->Location = System::Drawing::Point(15, 69);
			this->lb_file_save->Name = L"lb_file_save";
			this->lb_file_save->Size = System::Drawing::Size(172, 16);
			this->lb_file_save->TabIndex = 3;
			this->lb_file_save->Text = L"Ruta al Fichero Resultado";
			// 
			// lb_folder
			// 
			this->lb_folder->AutoSize = true;
			this->lb_folder->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lb_folder->Location = System::Drawing::Point(15, 37);
			this->lb_folder->Name = L"lb_folder";
			this->lb_folder->Size = System::Drawing::Size(178, 16);
			this->lb_folder->TabIndex = 2;
			this->lb_folder->Text = L"Ruta del Directorio a Listar";
			// 
			// bt_help_save_file
			// 
			this->bt_help_save_file->Location = System::Drawing::Point(615, 64);
			this->bt_help_save_file->Name = L"bt_help_save_file";
			this->bt_help_save_file->Size = System::Drawing::Size(28, 26);
			this->bt_help_save_file->TabIndex = 1;
			this->bt_help_save_file->Text = L"...";
			this->bt_help_save_file->UseVisualStyleBackColor = true;
			this->bt_help_save_file->Click += gcnew System::EventHandler(this, &Form1::bt_help_save_file_Click);
			// 
			// bt_help_folder
			// 
			this->bt_help_folder->Location = System::Drawing::Point(615, 32);
			this->bt_help_folder->Name = L"bt_help_folder";
			this->bt_help_folder->Size = System::Drawing::Size(28, 26);
			this->bt_help_folder->TabIndex = 0;
			this->bt_help_folder->Text = L"...";
			this->bt_help_folder->UseVisualStyleBackColor = true;
			this->bt_help_folder->Click += gcnew System::EventHandler(this, &Form1::bt_help_folder_Click);
			// 
			// bt_close
			// 
			this->bt_close->Location = System::Drawing::Point(468, 131);
			this->bt_close->Name = L"bt_close";
			this->bt_close->Size = System::Drawing::Size(77, 31);
			this->bt_close->TabIndex = 1;
			this->bt_close->Text = L"Cerrar";
			this->bt_close->UseVisualStyleBackColor = true;
			this->bt_close->Click += gcnew System::EventHandler(this, &Form1::bt_close_Click);
			// 
			// bt_list_files
			// 
			this->bt_list_files->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->bt_list_files->Location = System::Drawing::Point(551, 131);
			this->bt_list_files->Name = L"bt_list_files";
			this->bt_list_files->Size = System::Drawing::Size(104, 31);
			this->bt_list_files->TabIndex = 2;
			this->bt_list_files->Text = L"Listar";
			this->bt_list_files->UseVisualStyleBackColor = true;
			this->bt_list_files->Click += gcnew System::EventHandler(this, &Form1::bt_list_files_Click);
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(12, 131);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(421, 31);
			this->progressBar1->TabIndex = 6;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->ClientSize = System::Drawing::Size(677, 176);
			this->Controls->Add(this->progressBar1);
			this->Controls->Add(this->bt_list_files);
			this->Controls->Add(this->bt_close);
			this->Controls->Add(this->groupBox1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(685, 203);
			this->MinimumSize = System::Drawing::Size(685, 203);
			this->Name = L"Form1";
			this->Text = L"LIST-FILES FROM FOLDER";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}

#pragma endregion

	private: System::Void bt_help_folder_Click(System::Object^  sender, System::EventArgs^  e) { txb_folder->Text = PopFolderOpenHelp(); }
	private: System::Void bt_help_save_file_Click(System::Object^  sender, System::EventArgs^  e) { txb_file_save->Text = PopFileSaveHelp(); }
	private: System::Void bt_close_Click(System::Object^  sender, System::EventArgs^  e) { Form1::Close(); }
	private: System::Void bt_list_files_Click(System::Object^  sender, System::EventArgs^  e) { ListFilesToFile(); }
	private: System::String^ PopFolderOpenHelp()
			 {
				 if(folderBrowserDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)			
					 return folderBrowserDialog1->SelectedPath;	
				 return L"";
			 }

	private: System::String^ PopFileSaveHelp()
			 {		  
				 SaveFileDialog^ saveFileDialog1 = gcnew SaveFileDialog;
				 saveFileDialog1->Filter = "xls files (*.xls)|*.xls";
				 saveFileDialog1->FilterIndex = 2;
				 saveFileDialog1->RestoreDirectory = true;
				 if ( saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
					 return saveFileDialog1->FileName;											 				  
				 return L"";
			 }
	private: System::Boolean Check_FieldsInputs()
			 {
				 if (System::String::IsNullOrEmpty(txb_folder->Text) == true)
				 {
					 MessageBox::Show("La ruta del Directorio a Listar es OBLIGATORIA.");
					 return false;
				 }
				 if (System::String::IsNullOrEmpty(txb_file_save->Text) == true)
				 {
					 MessageBox::Show("La ruta del fichero Resultado es OBLIGATORIA.");
					 return false;
				 }

				 return true;
			 }

	private: System::Void ProcessFile( System::String^ path )
			 {

				 this->m_fileList->Add(path);
			 }

	private: System::Void ProcessDirectory( System::String^ targetDirectory )
			 {
				 using namespace System::IO;
				 // Process the list of files found in the directory.
				 array<String^>^fileEntries = Directory::GetFiles( targetDirectory );
				 IEnumerator^ files = fileEntries->GetEnumerator();
				 while ( files->MoveNext() )
				 {
					 String^ fileName = safe_cast<String^>(files->Current);
					 ProcessFile( fileName );
				 }


				 // Recurse into subdirectories of this directory.
				 array<String^>^subdirectoryEntries = Directory::GetDirectories( targetDirectory );
				 IEnumerator^ dirs = subdirectoryEntries->GetEnumerator();
				 while ( dirs->MoveNext() )
				 {
					 String^ subdirectory = safe_cast<String^>(dirs->Current);
					 ProcessDirectory( subdirectory );
				 }
			 }

	private: System::Void ListFilesToFile()
			 {
				 using namespace System::Runtime::InteropServices;
				 using namespace System::IO;				
				 System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				 if (Check_FieldsInputs())
				 {
					 this->m_fileList->Clear();
					 progressBar1->Visible = true;
					 progressBar1->Minimum = 1;
					 progressBar1->Maximum = 100;
					 progressBar1->Value = 1;
					 progressBar1->Step = 25;					 
					 progressBar1->PerformStep();
					 if ( Directory::Exists( txb_folder->Text ) )
						 ProcessDirectory(txb_folder->Text);
					 progressBar1->PerformStep();
					 if (this->m_fileList->Count > 0)
					 {
						 StreamWriter^ sw = File::CreateText( this->txb_file_save->Text );
						 try
						 {	
							for( int i = 0; i < this->m_fileList->Count; i++ )
							{								
								sw->WriteLine(this->m_fileList[i]);
							}
							progressBar1->PerformStep();
								
						 }
						 finally
						 {
							 if ( sw )
								 delete (IDisposable^)(sw);
							 progressBar1->PerformStep();
							 MessageBox::Show("Fichero generado con exito en : " + this->txb_file_save->Text );
						 }
					 }
					 else
					 {
						 progressBar1->PerformStep();
						 MessageBox::Show("No se ha generado ningun fichero, debido a que no se han encontrado ficheros. " );
					 }
				 }
				 System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
			 }

	};
}

